# == Schema Information
#
# Table name: beta_waiters
#
#  id          :bigint           not null, primary key
#  email       :string
#  emails_sent :integer
#  status      :string           default("not_in_beta")
#  url         :string
#
class BetaWaiter < ApplicationRecord
  extend Enumerize

  enumerize :status, in: [:not_in_beta, :in_beta]

  after_save :need_send_email

  def need_send_email

    if status == :in_beta
      BetaMailer.you_in_beta(self).deliver_now
    else
      self.worker_checker_beta
    end

  end

  def self.producer_checker_beta
    BetaWaiter.where(status: :not_in_beta).each do |beta_waiter|
      beta_waiter.worker_checker_beta
    end
  end
  
  def worker_checker_beta
    has_beta = false
    begin
      # url = "https://steamcommunity.com/id/artifactzone//games/?tab=all"

      url = "#{self.url}/games/?tab=all"
      doc = Nokogiri::HTML(open(url))

      begin
        has_beta = ':1269260,'.in?(doc.css('script[language="javascript"]').text)
      rescue
        has_beta = false
      end

      if has_beta
        self.update(status: :in_beta)
      end
    rescue
      has_beta = false
    end
  end

end
